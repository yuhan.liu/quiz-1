package com.twuc.webApp.model;

import javax.validation.constraints.NotNull;

public class Game {
    @NotNull
    private int gameId;
    private int answer;

    public Game(int gameId, int result) {
        this.gameId = gameId;
        this.answer = result;
    }

    public int getAnswer() {
        return answer;
    }

    public int getGameId() {
        return gameId;
    }
}
