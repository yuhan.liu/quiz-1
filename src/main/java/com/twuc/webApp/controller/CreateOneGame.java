package com.twuc.webApp.controller;

import com.twuc.webApp.model.Game;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CreateOneGame {

    private static Map<Integer, Game> gameMap = new HashMap<>();
    private int gameId;

    static Map<Integer, Game> getGameMap() {
        return gameMap;
    }
    @Bean
    public Game createGame(){
        Game game = new Game(++gameId,(int)((Math.random()*9+1)*1000));
        gameMap.put(gameId,game);
        return game;
    }
}
