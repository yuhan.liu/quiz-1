package com.twuc.webApp.controller;

import com.twuc.webApp.model.Game;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NewGameController {
    @PostMapping("/api/games")
    ResponseEntity return_game() {
        Game game = new CreateOneGame().createGame();
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location",
                        "/api/games/" + String.valueOf(game.getGameId()))
                .body("");
    }

    @GetMapping("/api/games/{gameId}")
    ResponseEntity returnGameStatus(@PathVariable int gameId) {
        Game game = CreateOneGame.getGameMap().get(gameId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("");
    }

    @PatchMapping("/api/games/{gameId}")
    ResponseEntity<String> returnFinalAnswer(@PathVariable int gameId){
        Game game = CreateOneGame.getGameMap().get(gameId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("");
    }
}
