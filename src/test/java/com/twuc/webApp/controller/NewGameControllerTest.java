package com.twuc.webApp.controller;

import com.twuc.webApp.model.Game;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class NewGameControllerTest {
    @Autowired
    MockMvc mockMvc;

    private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

    @Test
    void should_get_201_status_code() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    void should_get_game_url() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/games/1"));
    }

    @Test
    void should_get_game_status_info() throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        mockMvc.perform(get("/api/games/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Test
    void should_get_game_answer() throws Exception {
        Game game= context.getBean(Game.class);
        mockMvc.perform(patch("/api/games/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string(""));
    }
}